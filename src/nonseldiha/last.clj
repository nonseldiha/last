(ns nonseldiha.last
  (:require [cljfx.api :as fx]
            [clojure.java.io :as io]
            [clojure [edn :as edn] [string :as s]])
  (:import [java.nio.file Path]
           [java.io File]
           [javafx.scene.input MouseEvent]
           [javafx.scene.control TreeCell]))

(set! *warn-on-reflection* true)
(def *state (atom (fx/create-context {})))
(defmulti event-handler :event/type)

(defn resolve-deps-edn [fr]
  (with-open [deps-edn (-> (io/file fr "deps.edn") io/reader java.io.PushbackReader.)]
    {:deps-edn (edn/read deps-edn)}))
;; maybe we'll get the directories differently later on, we'll see
;; -Spath might be an option as well
;; or if an effective deps.edn can be built
;; also we didn't include the userwide deps.edn yet
(defn active-aliases [_cli-args] #{})
(defn +active-aliases [prj cli-args]
  (assoc prj :active-aliases (active-aliases cli-args)))
(defn source-file? [^File f]
  (and (.isFile f) (re-matches #"^.*\.clj[scx]?$" (str f))))
(defn folder? [f] (.isDirectory ^File f))
(defn source-files% [^File root]
  (reduce (fn [ac ^File nx]
            (if (source-file? nx) (conj ac nx)
                (if-some [rec (and (folder? nx) (source-files% nx))] (conj ac rec) ac)))
          [root] (-> root io/file .listFiles sort)))
(defn source-files [root]
  {:root root :files (source-files% (io/file root))})
(defn source-tree [{:keys [deps-edn active-aliases]}]
  (let [aliases (:aliases deps-edn)
        roots (sort (into (set (:paths deps-edn))
                          (comp (map #(% aliases)) (mapcat :extra-paths))
                          active-aliases))]
    (prn roots)
    (into [] (map source-files) roots)))
(defn +source-tree [prj]
  (assoc prj :source-tree (source-tree prj)))
(defn init-project-metadata [cli-args fr] ;TODO make fr optional
  (-> (resolve-deps-edn fr) (+active-aliases cli-args) +source-tree))
(defn source-files-desc [[fr & chs]]
  {:fx/type :tree-item :value fr
   :children (mapv #(if (vector? %) (source-files-desc %) {:fx/type :tree-item :value %}) chs)})
(defn sources-desc [{:keys [ctx]}]
  {:fx/type :tree-view
   :cell-factory
   {:fx/cell-type :tree-cell
    :describe (fn [^File x]
                {:text (.getName x)
                 :on-mouse-clicked
                 #(let [^TreeCell ti (.getSource ^MouseEvent %)
                        ^File f (-> ti .getTreeItem .getValue)]
                    (when (.isFile f) (println (slurp f))))})}
   :root (-> (init-project-metadata nil ".") :source-tree first :files source-files-desc)})
(defn root-desc [ctx]
  {:fx/type :stage :showing true :title "Last"
   :scene {:fx/type :scene
           :root {:fx/type :v-box
                  :children [{:fx/type sources-desc :ctx ctx}]}}})

(defn primary-desc [{:keys [ctx]}]
  {:fx/type :v-box
   :fill-width true
   :children [{:fx/type sources-desc :ctx ctx}
              #_{:fx/type :text-area :text "Primary View"}
              {:fx/type :text-area :text "Secondary View"}]})
(defn keyboard-desc [{:keys [ctx]}]
  {:fx/type :list-view
   :cell-factory {:fx/cell-type :list-cell
                  :describe (fn [x] {:text (str "Key " x)})}
   :items (range 10)})
(defn result-desc [{:keys [ctx]}]
  {:fx/type :v-box
   :children [{:fx/type :text-area :text "Result View"}
              {:fx/type :text-area :text "stdout"}
              {:fx/type :text-area :text "stderr"}]})
(defn root-desc2 [ctx]
  {:fx/type :stage :showing true :title "Last"
   :scene {:fx/type :scene
           :root {:fx/type :h-box
                  :children [{:fx/type primary-desc :ctx ctx}
                             {:fx/type keyboard-desc :ctx ctx}
                             {:fx/type result-desc :ctx ctx}]}}})
(defn -main []
  (fx/create-app *state :event-handler event-handler :desc-fn root-desc2))
(comment
  (-> (init-project-metadata nil ".") :source-tree first :files source-files-desc)
  (root-desc nil)
  (resolve-deps-edn ".")
  (-main)
  )
